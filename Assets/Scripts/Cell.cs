﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Cell 
{
    public Cell()
    {

    }

    public Cell(int x, int y, Vector2 zeroPosition)
    {
        CellNumber.X = x;
        CellNumber.Y = y;

        CellPosition = new Vector2(zeroPosition.x + ((x + 1) * Size - Size / 2), zeroPosition.y - ((y + 1) * Size - Size / 2));
        StartFieldX = CellPosition.x - Size / 2;
        StartFieldY = CellPosition.y - Size / 2;
        FinishFieldX = CellPosition.x + Size / 2;
        FinishFieldY = CellPosition.y + Size / 2;
    }

    public NumberInArray CellNumber = new NumberInArray();

    public Vector3 CellPosition = new Vector3();

    public float StartFieldX;
    public float StartFieldY;
    public float FinishFieldX;
    public float FinishFieldY;


    public float Size = 1f;

    [SerializeField]
    private List<CellContent> contents = new List<CellContent>() { new CellContent(ContentOfCell.None, null) };


    /// <summary>
    /// for none and obstacle
    /// </summary>
    /// <param name="type"></param>
    public bool SetContent(ContentOfCell type)
    {
        if (type != ContentOfCell.None && type != ContentOfCell.Obstacle)
        {
            return false;
        }

        contents.Clear();
        contents.Add(new CellContent(type, null));
        return true;
    }

    /// <summary>
    /// for food, bonuses, units
    /// </summary>
    /// <param name="type"></param>
    /// <param name="obj"></param>
    public bool SetContent(ContentOfCell type, DisintegrateUnit obj)
    {
        if (contents.Exists(x => x.Key == ContentOfCell.Obstacle))
        {
            return false;
        }

        if (contents.Exists(x => x.Key == ContentOfCell.None) && contents.Count == 1)
        {
            contents.Clear();
        }

        contents.Add(new CellContent(type, obj));
        return true;
    }

    public void GetContent(out List<ContentOfCell> types)
    {
        List<ContentOfCell> temp = new List<ContentOfCell>();

        foreach (CellContent item in contents)
        {
            temp.Add(item.Key);
        }

        types = temp;
    }

    public void GetContent(out List<CellContent> cellContents)
    {
        cellContents = contents;
    }

    /// <summary>
    /// for Player and Enemys
    /// </summary>
    /// <param name="obj"></param>
    public void RemoveContent(DisintegrateUnit obj)
    {
        if (contents.Exists(x => x.Value == obj))
        {
            CellContent temp = contents.Find(x => x.Value == obj);
            contents.Remove(temp);

            if (contents.Count == 0)
            {
                SetContent(ContentOfCell.None);
            }
        }
    }

    /// <summary>
    /// for Food and Bonuses
    /// </summary>
    /// <param name="type"></param>
    public void RemoveContent(ContentOfCell type)
    {
        if (contents.Exists(x => x.Key == type))
        {
            CellContent temp = contents.Find(x => x.Key == type);
            if (temp.Value != null)
            {
                temp.Value.Disintegrate();
            }
            contents.Remove(temp);
        }
    }
}

[Serializable] public class CellContent : SerializableKeyValuePair<ContentOfCell, DisintegrateUnit>
{
    public CellContent(ContentOfCell type, DisintegrateUnit obj)
    {
        Key = type;
        Value = obj;
    }
}

[Serializable]
public struct NumberInArray
{
    public NumberInArray(int x, int y)
    {
        X = x;
        Y = y;
    }

    public int X;
    public int Y;
}

public enum ContentOfCell
{
    None = 0,
    Player = 1,
    Obstacle = 2,
    Food = 3,
    Bonus = 4,
    Enemy = 5
}