﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Source
{
    public static IEnumerator DifferedAction(float delay, GameObject obj, Action call)
    {
        yield return new WaitForSeconds(delay);
        if (obj != null)
        {
            call();
        }
    }
}

public abstract class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviourSingleton<T>
{
    private static T instance;
    public static T Instance
    {
        get
        {
            if (!instance)
            {
                instance = FindObjectOfType<T>();
                if (!instance)
                {
                    GameObject gameObject = new GameObject(typeof(T).ToString());
                    instance = gameObject.AddComponent<T>();
                }
            }
            return instance;
        }
        protected set
        {
            if (!instance)
            {
                instance = value;
            }
            else if (value && instance != value)
            {
                Destroy(value);
            }
        }
    }

    /// <summary>
    /// обязательно вызвать базовай метод
    /// </summary>
    protected virtual void Awake()
    {
        Instance = this as T;
    }
}

[Serializable]
public static class Store
{
    public static int LevelNumber = 0;
}

[Serializable]
public struct levelConfigurator
{
    public int LevelNumber;

    // always 0,0
    public NumberInArray StartPlayerPosition;
    public int EnemysCount;
    public int BufferZone;

    public float EnemyCheckTargetDelay;

    public int X;
    public int Y;
}

[Serializable]
public class SerializableKeyValuePair<K, V>
{
    public K Key;
    public V Value;
}