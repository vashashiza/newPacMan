﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelCreator : MonoBehaviour
{
    [NonSerialized]
    public int FoodCount = 0;

    [SerializeField]
    private Transform background = null;

    [SerializeField]
    private DisintegrateUnit pieceOfFood = null;

    [SerializeField]
    private List<Enemy> enemysPrefabs = new List<Enemy>();

    [SerializeField]
    private BonusesContent bonusContent = new BonusesContent();

    [SerializeField]
    private CellVisualContent cellVisualContent = new CellVisualContent();


    private levelConfigurator currentLvl;

    private List<ColumnOfMap> cells;


    public void GenerateLvl(levelConfigurator currentLvl)
    {
        this.currentLvl = currentLvl;
        cells = Map.Instance.Cells;

        createMap();
        setFood();
    }

    public void SpawnEnemys(out List<Enemy> enemys)
    {
        enemys = new List<Enemy>();
        int x = currentLvl.StartPlayerPosition.X + currentLvl.BufferZone;
        int y = currentLvl.StartPlayerPosition.Y + currentLvl.BufferZone;

        for (int i = 0; i < currentLvl.EnemysCount; i++)
        {
            int index = UnityEngine.Random.Range(0, enemysPrefabs.Count);

            bool set = false;
            while (!set)
            {
                int xNum = UnityEngine.Random.Range(x, currentLvl.X);
                int yNum = UnityEngine.Random.Range(y, currentLvl.Y);

                if (!checkCell(ContentOfCell.Obstacle, xNum, yNum) && !checkCell(ContentOfCell.Enemy, xNum, yNum))
                {
                    Vector2 pos = Map.Instance.Cells[xNum].CellColumn[yNum].CellPosition;
                    Enemy enemy = Instantiate(enemysPrefabs[index], pos, new Quaternion());
                    set = Map.Instance.Cells[xNum].CellColumn[yNum].SetContent(ContentOfCell.Enemy, enemy);
                    enemy.ArrayPosition = new NumberInArray(xNum, yNum);
                    enemys.Add(enemy);
                }
            }
        }
    }

    public Bonus SpawnBonus(List<Cell> free)
    {
        int type = UnityEngine.Random.Range(0, 2);
        int cellNumber = UnityEngine.Random.Range(0, free.Count);

        int prefabIndex = 0;
        Bonus bonus = null;
        
        switch (type)
        {
            case 0:
                prefabIndex = UnityEngine.Random.Range(0, bonusContent.BonusesPrefabs.Count);
                bonus = Instantiate(bonusContent.BonusesPrefabs[prefabIndex], free[cellNumber].CellPosition, new Quaternion());
                break;
            case 1:
                prefabIndex = UnityEngine.Random.Range(0, bonusContent.AntibonusesPrefabs.Count);
                bonus = Instantiate(bonusContent.AntibonusesPrefabs[prefabIndex], free[cellNumber].CellPosition, new Quaternion());
                break;
        }

        bonus.Cell = free[cellNumber];

        //SetBonusSprite(bonus, type);
        setBonusToCell(free[cellNumber], bonus);

        return bonus;
    }

    private void setBonusToCell(Cell cell, Bonus bonus)
    {
        bool setBonus = cell.SetContent(ContentOfCell.Bonus, bonus);

        if (!setBonus)
        {
            Debug.LogError("can't set bonus");

        }
    }

   /* private void SetBonusSprite(Bonus bonus, int type)
    {
        SpriteRenderer sr = bonus.GetComponent<SpriteRenderer>();

        switch (type)
        {
            case 0:
                sr.sprite = bonusContent.BonusSprite;
                break;
            case 1:
                sr.sprite = bonusContent.AntibonusSprite;
                break;
        }
    }*/

    private void createMap()
    {
        background.localScale = new Vector2(currentLvl.X, currentLvl.Y);

        for (int i = 0; i < currentLvl.X + 2; i++)
        {
            for (int j = 0; j < currentLvl.Y + 2; j++)
            {
                GameObject cell = Instantiate(cellVisualContent.CellPrefab, cells[i].CellColumn[j].CellPosition, new Quaternion(), transform);
                
                SpriteRenderer sr = cell.GetComponent<SpriteRenderer>();

                if (checkCell(ContentOfCell.Obstacle, i, j))
                {
                    int index = UnityEngine.Random.Range(0, cellVisualContent.ObstacleSprites.Count);
                    sr.sprite = cellVisualContent.ObstacleSprites[index];
                }
                else
                {

                    int index = UnityEngine.Random.Range(0, cellVisualContent.CellsSprites.Count);
                    sr.sprite = cellVisualContent.CellsSprites[index];
                }
            }
        }
    }

    private void setFood()
    {
        for (int i = 0; i < currentLvl.X+2; i++)
        {
            for (int j = 0; j < currentLvl.Y+2; j++)
            {
                if (checkCell(ContentOfCell.None, i, j))
                {
                    DisintegrateUnit piece = Instantiate(pieceOfFood, cells[i].CellColumn[j].CellPosition, new Quaternion(), transform);

                    bool setFood = Map.Instance.Cells[i].CellColumn[j].SetContent(ContentOfCell.Food, piece);

                    if (!setFood)
                    {
                        Debug.LogError("can't set food");
                        
                    }
                    else
                    {
                        FoodCount++;
                    }
                }
            }
        }
    }    

    private bool checkCell(ContentOfCell checkType, int x, int y)
    {
        List<ContentOfCell> contents = new List<ContentOfCell>();
        Map.Instance.Cells[x].CellColumn[y].GetContent(out contents);
        
        if (contents.Exists(z => z == checkType))
        {
            return true;

        }
        else
        {
            return false;
        }

    }
}

[Serializable]
public struct BonusesContent
{
    /*public Sprite BonusSprite;
    public Sprite AntibonusSprite;
    public Sprite UnknownSprite;*/

    public List<Bonus> BonusesPrefabs;
    public List<Bonus> AntibonusesPrefabs;
}

[Serializable]
public struct CellVisualContent
{
    public GameObject CellPrefab;
    public List<Sprite> CellsSprites;
    public List<Sprite> ObstacleSprites;
}

