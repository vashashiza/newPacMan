﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGetBonuses : SetStateBonus
{
    protected override void active()
    {
        raiseOnActivetedStateBonus(this);
        Disintegrate();
    }
}
