﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingUnit : DisintegrateUnit
{   
    [SerializeField]
    protected float baseMoveSpeed = 2;
    [SerializeField]
    protected float maxMoveSpeed = 5;

    [SerializeField]
    protected bool canMove = true;

    [SerializeField]
    protected Vector3 target = new Vector2();

    [SerializeField]
    protected Cell targetCell;

    protected NumberInArray stepDirection = new NumberInArray();


    [SerializeField]
    protected NumberInArray arrayPosition = new NumberInArray();
    public NumberInArray ArrayPosition
    {
        get
        {
            return arrayPosition;
        }

        set
        {
            arrayPosition = value;
        }
    }
    [SerializeField]
    private float moveSpeed = 2;
    protected float MoveSpeed
    {
        get
        {
            return moveSpeed;
        }

        set
        {
            if (value > maxMoveSpeed)
            {
                moveSpeed = maxMoveSpeed;
            }
            else if (value < 0)
            {
                moveSpeed = 0;
            }
            else
            {
                moveSpeed = value;
            }
        }
    }

    public virtual void SetDirection(NumberInArray stepDirection)
    {
    }

    protected virtual void setNextPosition()
    {

    }

    protected virtual void checkPosition()
    {
        if (transform.position.x > targetCell.StartFieldX &&
            transform.position.x < targetCell.FinishFieldX &&
            transform.position.y < targetCell.FinishFieldY &&
            transform.position.y > targetCell.StartFieldY)
        {            
            changeMapPosition();
            setNextPosition();
        }

        move();
    }

    protected virtual void move()
    {
        float step = MoveSpeed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, target, step);
    }

    protected virtual void changeMapPosition()
    {
        
    }

    protected virtual void checkCellContent()
    {

    }

    private void Update()
    {
        if (canMove)
        {
            checkPosition();
        }        
    }
}