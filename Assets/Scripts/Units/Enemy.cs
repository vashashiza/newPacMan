﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : GetBonusUnit
{
    public bool InteractsWithBonuses = false;

    private int curretntStep = 1;
    private List<Cell> path = new List<Cell>();

    public virtual void ChangeInteracts(bool value)
    {
        if (this == null)
        {
            return;
        }

        InteractsWithBonuses = value;
    }

    public override void SetDirection(NumberInArray players)
    {
        Cell finishCell = Map.Instance.Cells[players.X].CellColumn[players.Y];
        Cell currentCell = Map.Instance.Cells[arrayPosition.X].CellColumn[arrayPosition.Y];
        path = Map.Instance.PathFinder.GetWay(finishCell, currentCell);

        if (path!=null)
        {
            curretntStep = path.Count - 1;
            setNextPosition();
        }
    }

    protected override void setNextPosition()
    {
        if (curretntStep > 0)
        {
            curretntStep--;
            targetCell = path[curretntStep];
            target = path[curretntStep].CellPosition;
            canMove = true;
        }
        else
        {
            canMove = false;
        }
        
    }

    protected override void changeMapPosition()
    {
        Map.Instance.Cells[ArrayPosition.X].CellColumn[ArrayPosition.Y].RemoveContent(this);

        arrayPosition = path[curretntStep].CellNumber;

        Map.Instance.Cells[arrayPosition.X].CellColumn[arrayPosition.Y].SetContent(ContentOfCell.Enemy, this);

        checkCellContent();
    }

    protected override void checkCellContent()
    {
        Cell cell = Map.Instance.Cells[ArrayPosition.X].CellColumn[ArrayPosition.Y];

        List<ContentOfCell> types = new List<ContentOfCell>();
        cell.GetContent(out types);

        if (types.Contains(ContentOfCell.Player))
        {
            /*
            List<CellContent> temp = new List<CellContent>();
            cell.GetContent(out temp);

            CellContent t = temp.Find(x => x.Key == ContentOfCell.Player);

            (t.Value as Player).SetDamage();*/
        }
    }

    private void Awake()
    {        
        canMove = false;
    }
}
