﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisintegrateUnit : MonoBehaviour
{
    public virtual void Disintegrate()
    {
        if ((object)this != null)
        {
            Destroy(gameObject);
        }
    }
}
