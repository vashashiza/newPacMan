﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetBonusUnit : MovingUnit
{
    public void UseBonus(Bonus bonus)
    {
        if ((object)this != null)
        {
            Coroutine effectCoroutine;

            switch (bonus.ActionType)
            {
                case BonusType.Speed:
                    if (bonus.Type == BoxType.Bonus)
                    {
                        MoveSpeed += (bonus as IncreaseBonus).BonusValue;
                    }
                    else
                    {
                        MoveSpeed -= (bonus as IncreaseBonus).BonusValue;
                    }

                    effectCoroutine = StartCoroutine(Source.DifferedAction(bonus.LifeTime, this.gameObject, delegate ()
                    {
                        MoveSpeed = baseMoveSpeed;
                    }));
                    break;
                case BonusType.Freeze:
                    canMove = false;

                    effectCoroutine = StartCoroutine(Source.DifferedAction(bonus.LifeTime, this.gameObject, delegate ()
                    {
                        canMove = true;
                    }));

                    break;
                case BonusType.Ghost:
                    BoxCollider2D collider = GetComponent<BoxCollider2D>();
                    collider.enabled = false;
                    effectCoroutine = StartCoroutine(Source.DifferedAction(bonus.LifeTime,  this.gameObject, delegate ()
                    {
                        collider.enabled = true;
                    }));
                    break;


                default:
                    break;
            }
        }
    }    
}
