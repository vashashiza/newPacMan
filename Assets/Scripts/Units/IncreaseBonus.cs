﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaseBonus : Bonus
{
    [SerializeField]
    private float bonusValue = 0f;

    public float BonusValue
    {
        get
        {
            return bonusValue;
        }
    }
}
