﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : GetBonusUnit
{
    public event Action<Cell> OnEat;

    public event Action OnMurder;

    public event Action<int> OnLoseLife;

    [SerializeField]
    protected string stepSound = "";
    [SerializeField]
    protected string eatSound = "";
    [SerializeField]
    protected string dieSound = "";

    [SerializeField]
    protected Animator animator = null;

    protected bool isAlive = true;

    [SerializeField]
    protected int lifes = 3;
    public int Lifes
    {
        get
        {
            return lifes;
        }

        set
        {
            if (isAlive)
            {
                if (value <= 0)
                {
                    death();
                }
                else
                {
                    lifes = value;
                }
            }
        }
    }

    /*
     public void SetDamage()
    {
      Lifes--;
    }*/

    public void Reset()
    {
        lifes = 3;
    }

    public override void SetDirection(NumberInArray stepDirection)
    {
        this.stepDirection = stepDirection;
        setNextPosition();
    }

    protected void Start()
    {
        checkCellContent();
    }
    private void Awake()
    {
        canMove = false;
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Enemy")
        {
            animator.SetBool("Dying", true);
            StartCoroutine(dyingAnimation());
            Lifes--;
            SoundManager.Instance.PlaySound(dieSound, false);
            raiseOnLoseLife(Lifes);
        }
    }
    protected IEnumerator dyingAnimation()
    {
        yield return new WaitForSeconds(0.5f);
        animator.SetBool("Dying", false);
    }
    protected override void setNextPosition()
    {
        NumberInArray nextPosition = new NumberInArray(ArrayPosition.X + stepDirection.X, ArrayPosition.Y + stepDirection.Y);

        if (ArrayPosition.X <0 || ArrayPosition.X> Map.Instance.Cells.Count-1 ||
            ArrayPosition.Y < 0 || ArrayPosition.Y > Map.Instance.Cells[0].CellColumn.Length - 1)
        {
            canMove = false;
            return;
        }

        Cell tempCell = Map.Instance.Cells[nextPosition.X].CellColumn[nextPosition.Y];

        targetCell = tempCell;
        target = tempCell.CellPosition;

        checkNextCell(nextPosition);
    }

    protected virtual void checkNextCell(NumberInArray nextPosition)
    {
        Cell temp = Map.Instance.Cells[nextPosition.X].CellColumn[nextPosition.Y];
        List<ContentOfCell> types = new List<ContentOfCell>();
        temp.GetContent(out types);

        if (!types.Contains(ContentOfCell.Obstacle))
        {
            canMove = true;
        }
        else
        {
            canMove = false;
        }
    }

    protected override void move()
    {
        base.move();
        SoundManager.Instance.PlaySound(stepSound, false);
    }

    protected override void changeMapPosition()
    {
        Map.Instance.Cells[ArrayPosition.X].CellColumn[ArrayPosition.Y].RemoveContent(this);

        arrayPosition.X += stepDirection.X;
        arrayPosition.Y += stepDirection.Y;        

        Map.Instance.Cells[arrayPosition.X].CellColumn[arrayPosition.Y].SetContent(ContentOfCell.Player, this);

        checkCellContent();
    }

    protected override void checkCellContent()
    {
        Cell cell = Map.Instance.Cells[ArrayPosition.X].CellColumn[ArrayPosition.Y];

        List<ContentOfCell> types = new List<ContentOfCell>();
        cell.GetContent(out types);

        if (types.Contains(ContentOfCell.Food))
        {
            cell.RemoveContent(ContentOfCell.Food);
            SoundManager.Instance.PlaySound(eatSound, false);
            raiseOnEat(cell);
        }
        /*
        if (types.Contains(ContentOfCell.Enemy))
        {
            Lifes--;
        }*/
    }

    protected void death()
    {
        isAlive = false;
        raiseOnMurder();

        Disintegrate();
    }

    protected void raiseOnEat(Cell cell)
    {
        if (OnEat != null) OnEat(cell);
    }

    protected void raiseOnLoseLife(int i)
    {
        if (OnLoseLife != null) OnLoseLife(i);
    }

    protected void raiseOnMurder()
    {
        if (OnMurder != null) OnMurder();
    }
}
