﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnknownBonuses : SetStateBonus
{
    protected override void active()
    {
        raiseOnActivetedStateBonus(this);
        Disintegrate();
    }

}
