﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : DisintegrateUnit
{
    public virtual event Action<Bonus> OnActivetedBonus;

    [SerializeField]
    protected Sprite baseSprite;
    [SerializeField]
    protected Sprite unknownSprite;

    public Cell Cell;

    [SerializeField]
    private float lifeTime = 0;
    public virtual float LifeTime
    {
        get
        {
            return lifeTime;
        }
    }

    [SerializeField]
    private BoxType type;
    public BoxType Type
    {
        get
        {
            return type;
        }
    }

    [SerializeField]
    private BonusType actionType;
    public BonusType ActionType
    {
        get
        {
            return actionType;
        }
    }

    protected GetBonusUnit target;

    public virtual void ChangeSprite(bool value)
    {
        if ((object)this == null)
        {
            return;
        }

        if (value)
        {
            if (this != null)
            {
                GetComponent<SpriteRenderer>().sprite = unknownSprite;
            }
        }
        else
        {
            if (this != null)
            {
                this.GetComponent<SpriteRenderer>().sprite = baseSprite;
            }            
        }
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            target = collision.GetComponent<GetBonusUnit>();
            active();
            raiseOnActivetedBonus();
        }
        else if (collision.tag == "Enemy")
        {
            if (collision.GetComponent<Enemy>().InteractsWithBonuses)
            {
                target = collision.GetComponent<GetBonusUnit>();
                active();
                raiseOnActivetedBonus();
            }
        }
    }

    protected virtual void active()
    {        
        target.UseBonus(this);
        Disintegrate();
    }

    protected virtual void raiseOnActivetedBonus()
    {
        if (OnActivetedBonus != null) OnActivetedBonus(this);
    }
}


public enum BoxType
{
    Bonus = 0,
    Antibonus = 1,
}

public enum BonusType
{
    Speed = 0,
    Freeze = 1,
    EnemyGetBonuses = 2,
    Ghost = 3,
    Unknown = 4,
}