﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetStateBonus : Bonus
{
    public event Action<Bonus> OnActivetedStateBonus;

    /*public virtual void SetBonusList(List<Bonus> activeBonuses)
    {

    }*/

    protected virtual void raiseOnActivetedStateBonus(Bonus bonus)
    {
        if (OnActivetedStateBonus != null) OnActivetedStateBonus(bonus);
    }
}
