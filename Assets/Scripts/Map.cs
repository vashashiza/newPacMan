﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map: MonoBehaviourSingleton<Map>
{
    public List<ColumnOfMap> Cells = new List<ColumnOfMap>();

    private Vector2 zeroPosition = new Vector2();

    private int x;

    private int y;

    [SerializeField]
    private PathFinder pathFinder;
    public PathFinder PathFinder
    {
        get
        {
            return pathFinder;
        }
    }

    public void GenerateLvl(int setX, int setY)
    {
        /*int x = setX + 2;
        int y = setY + 2;*/

        Cells.Clear();

        x = setX + 2;
        y = setY + 2;

        zeroPosition = new Vector2(-x / 2, y / 2);

        for (int i = 0; i < x; i++)
        {
            ColumnOfMap column = new ColumnOfMap(y);
            Cells.Add(column);
        }

        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                //Debug.Log(i + "," + j);
                Cells[i].CellColumn[j] = new Cell(i, j, zeroPosition);
            }
        }

        setObstacle();

        pathFinder = new PathFinder(this);
    }

    private void setObstacle()
    {
        setBorder();

        for (int j = 2; j < x; j += 2)
        {
            for (int i = 2; i < y; i += 2)
            {
                Cells[j].CellColumn[i].SetContent(ContentOfCell.Obstacle);
            }
        }
    }

    private void setBorder()
    {
        for (int i = 0; i < x; i++)
        {
            Cells[i].CellColumn[0].SetContent(ContentOfCell.Obstacle);
            Cells[i].CellColumn[y-1].SetContent(ContentOfCell.Obstacle);         
        }

        for (int i = 1; i < y-1; i++)
        {
            Cells[0].CellColumn[i].SetContent(ContentOfCell.Obstacle);
            Cells[x-1].CellColumn[i].SetContent(ContentOfCell.Obstacle);
        }
    }
}

[Serializable]
public class ColumnOfMap
{
    public Cell[] CellColumn;

    public ColumnOfMap()
    {

    }

    public ColumnOfMap(int lenght)
    {
        CellColumn = new Cell[lenght];

        for (int i = 0; i < lenght; i++)
        {
            CellColumn[i] = new Cell();
        }
    }
}