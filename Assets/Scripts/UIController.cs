﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    public event Action<int> OnUserInput;

    [SerializeField]
    private List<GameObject> lifes = new List<GameObject>();

    [SerializeField]
    private GameObject gameOverPanel = null;

    [SerializeField]
    private GameObject winPanel = null;

    //Inspector func
    public void ButtonClick(int i)
    {
        switch (i)
        {
            case 0:
                raiseOnUserInput(0);
                break;
            case 1:
                raiseOnUserInput(1);
                break;
        }
    }

    public void GameOver()
    {
        gameOverPanel.SetActive(true);
    }

    public void Win()
    {
        winPanel.SetActive(true);
    }

    public void Reset()
    {
        gameOverPanel.SetActive(false);
        winPanel.SetActive(false);

        foreach (GameObject item in lifes)
        {
            item.SetActive(true);
        }
    }

    public void RemoveLife(int i)
    {
        lifes[i].SetActive(false);
    }

    private void raiseOnUserInput(int i)
    {
        if (OnUserInput != null) OnUserInput(i);
    }
}
