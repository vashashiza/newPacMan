﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class PathFinder 
{
    [SerializeField]
    private List<PathNode> pathNodes;

    private Map map;

    public PathFinder()
    {

    }

    public PathFinder(Map map)
    {
        this.map = map;

        fillNodes();
    }

    private bool find = false;

    List<PathNode> uncheckedNodes = new List<PathNode>();
    List<PathNode> checkedNodes = new List<PathNode>();

    List<PathNode> currentIteration = new List<PathNode>();
    List<PathNode> nextIteration = new List<PathNode>();

    public List<Cell> GetWay(Cell target, Cell start)
    {
        List<Cell> temp = new List<Cell>();
        clearPathNodes();
        currentIteration.Clear();

        uncheckedNodes = new List<PathNode>(pathNodes);        

        PathNode first = pathNodes.Find(x => x.Cell == start);
        first.Previous = null;
        first.Coast = 0;
        currentIteration.Add(first);


        int coast = 0;

        while (!find || uncheckedNodes.Count >0)
        {
            coast++;
            foreach (PathNode item in currentIteration)
            {
                temp = checkNode(item, target, coast);

                if (temp!=null)
                {
                    return temp;
                }
            }

            currentIteration = new List<PathNode>(nextIteration);
            nextIteration.Clear();
        }

        return null;  
    }

    private List<Cell> checkNode(PathNode currentNode, Cell target, int coast)
    {       
        if (currentNode.Cell == target)
        {
            find = true;
            return getPathForNode(currentNode);
        }

        currentNode.Checked = true;
        uncheckedNodes.Remove(currentNode);
        checkedNodes.Add(currentNode);        

        foreach (PathNode item in currentNode.Neighbors)
        {
            if (item.Checked)
            {
                continue;
            }

            item.Previous = currentNode;
            item.Coast = coast;
            nextIteration.Add(item);
        }

        return null;
    }

    private List<Cell> getPathForNode(PathNode finish)
    {
        List<Cell> path = new List<Cell>();
        PathNode currentNode = finish;

        while (currentNode != null)
        {
            path.Add(currentNode.Cell);
            currentNode = currentNode.Previous;
        }

        return path;
    }

    private void clearPathNodes()
    {
        foreach (PathNode item in pathNodes)
        {
            item.Clear();
        }
    }

    private void fillNodes()
    {
        List<PathNode> pathNodes = new List<PathNode>();

        for (int i = 0; i < map.Cells.Count; i++)
        {
            Cell[] cells = map.Cells[i].CellColumn;
            for (int j = 0; j < cells.Length; j++)
            {
                Cell cell = cells[j];

                PathNode pathNode = new PathNode(cell)
                {
                    Neighbors = pathNodes.FindAll(item =>
                    ((item.Cell.CellNumber.X == cell.CellNumber.X+1 && 
                    item.Cell.CellNumber.Y == cell.CellNumber.Y) ||
                    (item.Cell.CellNumber.X == cell.CellNumber.X - 1 &&
                    item.Cell.CellNumber.Y == cell.CellNumber.Y) ||
                    (item.Cell.CellNumber.X == cell.CellNumber.X &&
                    item.Cell.CellNumber.Y == cell.CellNumber.Y+1) ||
                    (item.Cell.CellNumber.X == cell.CellNumber.X &&
                    item.Cell.CellNumber.Y == cell.CellNumber.Y - 1)
                    ))                   
                };               

                List<ContentOfCell> types = new List<ContentOfCell>();
                pathNode.Cell.GetContent(out types);
                if (types.Exists(x => x == ContentOfCell.Obstacle))
                {
                    continue;
                }

                pathNode.NeighborsCount = pathNode.Neighbors.Count;

                foreach (PathNode item in pathNode.Neighbors)
                {
                    addOnce(item.Neighbors, pathNode);
                    item.NeighborsCount = item.Neighbors.Count;
                }

                pathNodes.Add(pathNode);
            }
        }

        this.pathNodes = pathNodes;
    }

    private bool addOnce(List<PathNode> list, PathNode item)
    {
        if (!list.Contains(item))
        {
            list.Add(item);
            return true;
        }
        return false;
    }

    [Serializable]
    private class PathNode
    {
        public Cell Cell = null;

        public int NeighborsCount = 0;

        [NonSerialized]
        public List<PathNode> Neighbors = new List<PathNode>();

        public PathNode Previous;

        public bool Checked = false;

        public int Coast = 0;

        public PathNode(Cell cell)
        {
            Cell = cell;
        }

        public void Clear()
        {
            Checked = false;
            Previous = null;
        }
    }
}
