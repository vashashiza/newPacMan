﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField]
    private LevelCreator levelCreator = null;

    [SerializeField]
    private UIController uiController = null;

    [SerializeField]
    private List<levelConfigurator> levels = new List<levelConfigurator>();

    [SerializeField]
    private Player player = null;

    [SerializeField]
    private List<Enemy> enemys = new List<Enemy>();

    [SerializeField]
    private List<Bonus> bonuses = new List<Bonus>();

    [SerializeField]
    private string backgroundSound = "";

    [SerializeField]
    private float minSpawnBonusDelay = 2;
    [SerializeField]
    private float maxSpawnBonusDelay = 5;

    private int foodCount = 0;

    private levelConfigurator currentLvl;

    private List<Cell> freeCell = new List<Cell>();

    private void Awake()
    {
        Time.timeScale = 1;
        uiController.OnUserInput += uiController_OnUserInput;

        currentLvl = levels.Find(z => z.LevelNumber == Store.LevelNumber);
        Map.Instance.GenerateLvl(currentLvl.X, currentLvl.Y);
        levelCreator.GenerateLvl(currentLvl);

        configuratePlayer(currentLvl);

        spawnEnemys();

    }

    private void Start()
    {
        SoundManager.Instance.PlaySound(backgroundSound, true);
        foodCount = levelCreator.FoodCount;

        StartCoroutine(spawnBonus());
    }

    private void uiController_OnUserInput(int i)
    {
        switch (i)
        {
            case 0:
                reload();
                break;
            case 1:
                nextLvl();
                break;
        }
    }

    private IEnumerator spawnBonus()
    {
        while (true)
        {
            float delay = UnityEngine.Random.Range(minSpawnBonusDelay, maxSpawnBonusDelay);

            if (freeCell.Count <= 0)
            {
                yield return new WaitForSeconds(delay);
                continue;
            }

            Bonus bonus = levelCreator.SpawnBonus(freeCell);
            bonuses.Add(bonus);

            freeCell.Remove(bonus.Cell);

            bonus.OnActivetedBonus += Bonus_OnActivetedBonus;

            if (bonus.GetComponent<SetStateBonus>() != null)
            {
                bonus.GetComponent<SetStateBonus>().OnActivetedStateBonus += GameController_OnActivetedStateBonus;
            }

            yield return new WaitForSeconds(delay);
        }     
    }

    private void GameController_OnActivetedStateBonus(Bonus currentBonus)
    {

        switch (currentBonus.ActionType)
        {
            case BonusType.EnemyGetBonuses:

                if (enemys.Count < 0)
                {
                    return;
                }

                foreach (Enemy item in enemys)
                {
                    if (item == null)
                    {
                        continue;
                    }

                    item.ChangeInteracts(true);
                    /*StartCoroutine(Source.DifferedAction(currentBonus.LifeTime, item.gameObject, delegate ()
                    {
                        item.ChangeInteracts(false);
                    }));*/
                }
                break;
            case BonusType.Unknown:

                if (bonuses.Count < 0)
                {
                    return;
                }

                foreach (Bonus item in bonuses)
                {
                    if (item == null)
                    {
                        continue;
                    }

                    item.ChangeSprite(true);
                    StartCoroutine(Source.DifferedAction(currentBonus.LifeTime, item.gameObject, delegate ()
                    {
                        item.ChangeSprite(false);
                    }));
                }
                break;
        }
    }

    private void Bonus_OnActivetedBonus(Bonus obj)
    {
        freeCell.Add(obj.Cell);
        bonuses.Add(obj);
    }

    private void spawnEnemys()
    {        
        levelCreator.SpawnEnemys(out enemys);
        setTargetEnemys();
        StartCoroutine(setTarget());
    }

    private IEnumerator setTarget()
    {
        while (true)
        {
            yield return new WaitForSeconds(currentLvl.EnemyCheckTargetDelay);
            setTargetEnemys();
        }
    }

    private void setTargetEnemys()
    {
        foreach (Enemy item in enemys)
        {
            item.SetDirection(player.ArrayPosition);
        }
    }

    private void configuratePlayer(levelConfigurator currentLvl)
    {
        int x = currentLvl.StartPlayerPosition.X;
        int y = currentLvl.StartPlayerPosition.Y;
        player.transform.position = Map.Instance.Cells[x].CellColumn[y].CellPosition;

        player.ArrayPosition = new NumberInArray(x, y);

        player.gameObject.SetActive(true);

        player.OnEat += Player_OnEat;

        player.OnMurder += Player_OnMurder;

        player.OnLoseLife += Player_OnLoseLife;
    }

    private void Player_OnLoseLife(int i)
    {
        uiController.RemoveLife(i);
    }

    private void Player_OnMurder()
    {
        gameOver();
    }

    private void Player_OnEat(Cell cell)
    {
        freeCell.Add(cell); 

        foodCount--;

        if (foodCount == 0)
        {
            win();
        }        
    }

    private void nextLvl()
    {
        if (Store.LevelNumber < levels.Count)
        {
            Store.LevelNumber++;
        }

        reload();
    }

    private void reload()
    {
        int i = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(i); 
    }

    private void win()
    {
        Time.timeScale = 0;
        player.gameObject.SetActive(false);
        uiController.Win();
    }

    private void gameOver()
    {
        Time.timeScale = 0;
        player.gameObject.SetActive(false);
        uiController.GameOver();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            player.SetDirection(new NumberInArray(-1, 0));
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            player.SetDirection(new NumberInArray(1, 0));
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            player.SetDirection(new NumberInArray(0, -1));
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            player.SetDirection(new NumberInArray(0, 1));
        }
    }
}